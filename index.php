<!--
/*==============
D1: APPLICATION
1) Transfer header to nav to partials > header.php file
2) Transfer script tags, closing body tag and closing html tag to partials > footer.php file

Only the container will remain in index.php file. Create the following pages and apply concepts taught above.

cart.php
login.php
register.php
product.php

===============*/ -->

<?php 
	/*==========
	D1: INCLUDE & REQUIRE
	============*/
	require "partials/header.php";

?>


<!-- Sections -->
<section class="mb-5">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="card bg-warning text-white text-center">
				  <div class="card-body">
				    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    This call to action - this is some text within a card body - clever tagline!
				  </div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php 
//1) GET PRODUCTS FROM JSON FILE
	// echo "<pre>";
	// echo file_get_contents("assets/lib/products.json");
	// echo "</pre>";
	$products = file_get_contents("assets/lib/products.json");
	echo "<hr>";

//2) CONVERT CONTENTS TO PHP ARRAY
	$productsArray = json_decode($products, true);	//true - converts returned objects into assoc array
	//echo "$studentsArray["name"]";
	// echo "<pre>";
	// print_r($productsArray);
	// echo "</pre>";
?>

<!-- Sections -->
<section class="mb-5">
	<div class="container">
		<div class="row">
		    <div class="col-lg-4 col-md-6">
		        <div class="card">	        	
				  	<?php 
				  		foreach($productsArray as $product){
				  			echo '<img src = "'.$product["image"].'">';
				  	?>

					         <div class="card-body">
										<h4 class="card-title">Title: <?php echo $product["name"]; ?></h4>
										<p class="card-text">Price: <?php echo $product["price"]; ?></p>
										<p class="card-text">Desc: <?php echo $product["description"]; ?></p>
										<!-- Create add product button and quantity text box. Discuss in class -->
										<form method="POST" action="processAddToCart.php?customerId=<?php echo $i ?>">
										    <input type="number" name="quantity" class="form-control">
										    <button type="submit" class="btn btn-success btn-lg btn-block form-control">
										        <i class="fas fa-plus-circle"></i> Add to Cart
										    </button>
										</form>
							</div>
					<?php 
				  		}
				  	 ?>
		            </div>
		        </div>
		    </div>
		</div>
</section>


<?php 
require "partials/footer.php";
?>


<!--
/*==============
D2: APPLICATION
1. Open assets/lib/products.json and create an array with six objects.
2. Each object must contain the properties: name, price, description, and image.
3. the images are inside images folder which is a sibling of products.json file
4. Use above discussion to display the products in the index.php file
5. Use the card structure below to contain the products
===============*/
-->

<!-- 
/*==============
D3: Application

1. Create Add To Cart functionality
    - Inside lib folder, create processAddToCart.php
2. Display message in index.php when items are added to cart
    - This will require you to check if session message is set.
===============*/
 -->