<?php

	// echo $_POST["payment"];
	// echo "<br>";
	// echo $_GET["studentId"];

	/*====================
	D3 PHP SESSION

	storage of variables to be used across multiple pages
	=====================*/

	//1) START A SESSION
	session_start();

	$quantity =  $_POST["quantity"];
	echo "Quantity: $quantity";
	echo "<br>";
	$student_id = $_GET["customerId"];
	echo "Customer id: $customer_id";
	echo "<br>";

	//2 SET SESSION VARIABLE
	//check first if session is set. if it is not, set it. otherwise, update the current data;
	if(isset($_SESSION["account"][$customer_id])){
		$_SESSION["account"][$customer_id] += $quantity;		//notice the '+='
	} else {
		$_SESSION["account"][$customer_id] = $quantity;		//notice the '='
	}

	//var_dump($_SESSION);

	//what: notify user that payment is received. start another session, call it 'message'.
	$_SESSION["message"] = "$quantity has been received.";
	$message = $_SESSION["message"];
	header("Location: index.php");		//

?>
