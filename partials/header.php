<!DOCTYPE html>
<html ang="en">
<head>
	<!-- META TAGS -->
	<meta charset="utf-8">
	<!-- DISABLE VIEWPORT ZOOM -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="IE-Edge">
	<title>Bootstrap Components</title>

	<!-- FONT AWESOME -->
	<script src="https://kit.fontawesome.com/ac74f518bc.js"></script>

	<!-- BOOTSTRAP CSS -->
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
	<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css"></head>

	<!-- EXTERNAL CSS -->
	<link rel="stylesheet" type="text/css" href="assets/css/style.css">
</head>
<body>


	<!-- NAVBAR -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-warning fixed-top">
		<div class="container">
		  <a class="navbar-brand font-weight-bold" href="#">John&Lemons</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarSupportedContent">
		    <ul class="navbar-nav ml-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="index.html">Login<span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="about.html">Register</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="services.html">Cart</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="contact.html">Logout</a>
		      </li>
		    </ul>

		  </div>
		</div>
	</nav>